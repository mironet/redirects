# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [v0.5.3] - 2025-02-19

### Fixed

- Fix: Handle X-Forwarded-... header family correctly — again 🤦‍♂️

## [v0.5.2] - 2025-02-03

### Fixed

- Fix: Handle X-Forwarded-... header family correctly.

## [v0.5.1] - 2025-01-31

### New Features

- Add support for GeoIP matching of rules.

## [v0.3.1] - 2023-01-23

### Fixed

- Fix: Handle non-ascii urls correctly.
