package main

import (
	"crypto/tls"
	"net/http"
	"net/http/httputil"
	"testing"
)

func createTestProxyRequest(inReq *http.Request) *httputil.ProxyRequest {
	outReq := &http.Request{
		Header: make(http.Header),
	}
	return &httputil.ProxyRequest{
		In:  inReq,
		Out: outReq,
	}
}

func TestSetXForwarded(t *testing.T) {
	tests := []struct {
		name              string
		setupRequest      func() *http.Request
		expectedForwarded string
		expectedProto     string
		expectedHost      string
		shouldHaveSavedIP bool
	}{
		{
			name: "basic forwarding",
			setupRequest: func() *http.Request {
				req := &http.Request{
					Header: make(http.Header),
				}
				req.Header.Set(savedRemoteIP, "192.168.1.1:12345")
				return req
			},
			expectedForwarded: "192.168.1.1",
			expectedProto:     "http",
			expectedHost:      "",
			shouldHaveSavedIP: false,
		},
		{
			name: "with existing X-Forwarded-For",
			setupRequest: func() *http.Request {
				req := &http.Request{
					Header: make(http.Header),
				}
				req.Header.Set(savedRemoteIP, "192.168.1.2:12345")
				req.Header.Set("X-Forwarded-For", "10.0.0.1")
				return req
			},
			expectedForwarded: "10.0.0.1, 192.168.1.2",
			expectedProto:     "http",
			expectedHost:      "",
			shouldHaveSavedIP: false,
		},
		{
			name: "with invalid IP",
			setupRequest: func() *http.Request {
				req := &http.Request{
					Header: make(http.Header),
				}
				req.Header.Set(savedRemoteIP, "invalid-ip")
				return req
			},
			expectedForwarded: "",
			expectedProto:     "http",
			expectedHost:      "",
			shouldHaveSavedIP: false,
		},
		{
			name: "with HTTPS",
			setupRequest: func() *http.Request {
				req := &http.Request{
					Header: make(http.Header),
					TLS:    &tls.ConnectionState{},
				}
				req.Header.Set(savedRemoteIP, "192.168.1.3:12345")
				return req
			},
			expectedForwarded: "192.168.1.3",
			expectedProto:     "https",
			expectedHost:      "",
			shouldHaveSavedIP: false,
		},
		{
			name: "with existing X-Forwarded-Proto",
			setupRequest: func() *http.Request {
				req := &http.Request{
					Header: make(http.Header),
				}
				req.Header.Set(savedRemoteIP, "192.168.1.4:12345")
				req.Header.Set("X-Forwarded-Proto", "https")
				return req
			},
			expectedForwarded: "192.168.1.4",
			expectedProto:     "https",
			expectedHost:      "",
			shouldHaveSavedIP: false,
		},
		{
			name: "with host",
			setupRequest: func() *http.Request {
				req := &http.Request{
					Header: make(http.Header),
					Host:   "example.com",
				}
				req.Header.Set(savedRemoteIP, "192.168.1.5:12345")
				return req
			},
			expectedForwarded: "192.168.1.5",
			expectedProto:     "http",
			expectedHost:      "example.com",
			shouldHaveSavedIP: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			inReq := tt.setupRequest()
			pr := createTestProxyRequest(inReq)

			setXForwarded(pr)

			// Check X-Forwarded-For
			got := pr.Out.Header.Get("X-Forwarded-For")
			if got != tt.expectedForwarded {
				t.Errorf("X-Forwarded-For = %v, want %v", got, tt.expectedForwarded)
			}

			// Check X-Forwarded-Proto
			got = pr.Out.Header.Get("X-Forwarded-Proto")
			if got != tt.expectedProto {
				t.Errorf("X-Forwarded-Proto = %v, want %v", got, tt.expectedProto)
			}

			// Check X-Forwarded-Host
			got = pr.Out.Header.Get("X-Forwarded-Host")
			if got != tt.expectedHost {
				t.Errorf("X-Forwarded-Host = %v, want %v", got, tt.expectedHost)
			}

			// Verify saved remote IP was cleaned up
			if tt.shouldHaveSavedIP != (pr.Out.Header.Get(savedRemoteIP) != "") {
				t.Logf("pr.Out.Header: %v", pr.Out.Header)
				t.Errorf("savedRemoteIP presence = %v, want opposite", tt.shouldHaveSavedIP)
			}
		})
	}
}
