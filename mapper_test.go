package main

import (
	"strings"
	"testing"
)

func TestMapperReloadBackwardCompatibility(t *testing.T) {
	csvData := `Source,Target,Code
https://example.com/old,https://target.com,302`

	m := new(mapper)
	err := m.Reload(strings.NewReader(csvData))

	if err != nil {
		t.Errorf("Expected no error with old CSV format, got %v", err)
	}
}
func TestSplitHostNamespace(t *testing.T) {
	tests := []struct {
		name          string
		input         string
		wantHost      string
		wantNamespace string
	}{
		{
			name:          "simple service and namespace",
			input:         "myservice.mynamespace",
			wantHost:      "myservice",
			wantNamespace: "mynamespace",
		},
		{
			name:          "full domain name",
			input:         "myservice.mynamespace.svc.cluster.local",
			wantHost:      "myservice",
			wantNamespace: "mynamespace",
		},
		{
			name:          "single part",
			input:         "myservice",
			wantHost:      "myservice",
			wantNamespace: "",
		},
		{
			name:          "empty string",
			input:         "",
			wantHost:      "",
			wantNamespace: "",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			host, namespace := splitHostNamespace(tt.input)
			if host != tt.wantHost {
				t.Errorf("splitHostNamespace() host = %v, want %v", host, tt.wantHost)
			}
			if namespace != tt.wantNamespace {
				t.Errorf("splitHostNamespace() namespace = %v, want %v", namespace, tt.wantNamespace)
			}
		})
	}
}
