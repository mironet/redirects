package main

import (
	"fmt"
	"testing"
)

func TestLoggingLevels(t *testing.T) {
	var scream = func() {
		logTrace("This is a trace message")
		logDebug("This is a debug message")
		logInfo("This is an info message")
		logWarn("This is a warning message")
		logError("This is an error message")
	}
	for i := 0; i <= levelError; i++ {
		loglevel = i
		fmt.Println("starting loglevel", i)
		scream()
		fmt.Println("ending loglevel", i)
	}
}
