# Redirects

Small server responding with redirects and proxying requests not found in the
redirect list to a predefined backend (acting as a proxy).

## Usage

Run inside Kubernetes and specify the rules file as well as the upstream target
as a hostname and port if needed.

The rules.csv file is best mounted by a ConfigMap. You can also use a gzipped
file, it will automatically be detected. If there is a large amount of redirects
to be processed, that might help not running into the 1 MiB ConfigMap limit.

## Regex

It is possible to use [RE2 syntax](https://github.com/google/re2/wiki/Syntax)
regexes in the source column of the redirect rules file. They will be evaluated
in order of appearance, first match wins.

So if you want a catch-all, specify it like this:

```csv
Source,Target,Code
https://www.example.com/,https://www.example.be/foo.html,308
https://www.example.com/bar,https://www.example.be/baz.html,308
https://www.example.com/[0-9]{2}(bar|baz),https://www.example.be/barbaz.html,308
https://www.example.com/.*,https://www.example.be/star.html,308
```

## Source IP Geolocation (Country)

With the use of a GeoIP database (from [DB-IP.com](https://db-ip.com)), the
server can redirect based on the country of the client. The database is loaded
at startup and a default is embedded. To make use of it, add an additoinal
column to the rules file:

```csv
Source,Target,Code,Country
https://www.example.com/,https://www.example.be/foo.html,308,BE
https://www.example.com/bar,https://www.example.de/bar.html,308,DE
https://www.example.com/bar,https://www.example.fr/bar.html,308,FR
```

In this example a call to `https://www.example.com/bar` from a client in Germany
will be redirected to `https://www.example.de/bar.html`. For a client in France
the target will be `https://www.example.fr/bar.html`.

The server will then redirect to the target if the client's country matches the
one in the rule. The country code is a two-letter ISO code. If the country code
is not found in the database, or the IP cannot be matched to a country, the rule
is ignored.

The `Country` column can also contain a list of country codes separated by a
comma. In that case, the rule will match if the client's country is in the list:

```csv
Source,Target,Code,Country
https://www.example.com/,https://www.example.fr/foo.html,308,"FR,BE,LU,CA"
```

Note the escaping of the commas with double quotes.


## Proxy, Forward, Redirect

The server can act as a proxy, forwarder or redirector. The default is
redirector. Each rule in the rules file can specify the mode to use. Either you
specify a code (as an integer) for the redirection, or you use the special words
`proxy` or `forward` in the `Code` field. If you use `proxy`, the server will
act as a reverse proxy and forward the request to the upstream target.

Outside of a Kubernetes cluster, `proxy` and `forward` are equivalent. Inside a
Kubernetes cluster, `proxy` will use the service name and port to forward the
request, while `forward` will use the target hostname and port directly. This is
useful if you want to forward to a service outside of the cluster.

See the following example:

```csv
Source,Target,Code
https://www.example.com/,https://www.example.be/foo.html,308
https://www.example.com/bar,http://internal-k8s-service-name:8080,proxy
https://www.example.com/baz,https://www.example.be,forward
```

You can also use regexes in the target field, they will be replaced with the
captured groups from the source regex. This is useful if you want to forward
requests to a service that expects a different path than the one in the source
URL.

```csv
https://www.example.com/test/(.*),http://localhost:9090/website/$1/baz,forward
https://www.example.com/(.+)/(.*),http://testing-service:80/$1/$2,proxy
```

> **Note:** The server does not rewrite the response body (with URLs in it based
> on a redirect rule). This would be too expensive and tends to create chaos.
> Use the target/backend application to handle these scenarios or a full-blown
> API/HTTP gateway if that's what you need.

## RBAC

In a k8s cluster, the redirect proxy needs specific access to the API. The
information is provided in the `ServiceAccount` resource
[here](k8s/serviceaccount.yml).

### Cross-Namespace Access

If you want to access services in other namespaces, you need to create a
`ClusterRole` and `RoleBinding`s in all namespaces involved (the ones where the
redirect server is running and the target service namespaces):

```bash
kubectl apply -f k8s/clusterrole.yml
```

See [the file above](k8s/clusterrole.yml) for an example in the comments.

## Example K8s Objects

See the [k8s directory](k8s/) for an example deployment. Create the config map
containing the redirect rules with:

```bash
kubectl create configmap redirects-config --from-file=redirects/rules.csv
```

## Load Balancing Modes

The server watches the upstream service endpoints and load-balances
automatically between them. The default method is round-robin but cookie-based
hashing is also possible:

```text
  -cookie string
        Name of the cookie to use for load-balancing if lb mode = cookie. (default "monitor-cookie")
  -lb string
        Load-balancing algorithm to use (round-robin, cookie). (default "round-robin")
```

## Performance

There is a benchmark test in redirects_test.go. It shows that the server can
handle hunderds of thousands of redirect rules and still respond rather quickly.
Most of the CPU time is spent in the proxying code anyway, not in the redirect
matching.

## Licenses

MIT

[IP Geolocation by DB-IP.com](https://db-ip.com)
