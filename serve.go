package main

import (
	"net/http"
	"time"
)

var (
	readyz int32
)

func newServer(address string, routes http.Handler) (*http.Server, error) {
	mux := http.NewServeMux()
	mux.Handle("/", routes)

	srv := &http.Server{
		Handler:      mux,
		Addr:         address,
		WriteTimeout: time.Hour * 24, // This is just a safety-net to *eventually* kill a slow client.
		ReadTimeout:  time.Second * 15,
	}

	return srv, nil
}
