package main

import (
	"hash/crc32"
	"net/http"
	"sync"
	"sync/atomic"
)

type lbfactory func(scheme string, port uint16) LoadBalancer // Create new loadbalancers based on the loadbalancing algorithm.
type backendCallback func(backends []string)                 // Called when backends are updated, with the list of backend urls.

type LoadBalancer interface {
	http.Handler
	SetBackends([]http.Handler)
}

type RoundRobinProxy struct {
	backends []http.Handler
	total    int64
	mux      sync.RWMutex // Protects backends and total.

	n      int64
	scheme string
	port   uint16
	cf     backendCallback
}

func NewRoundRobinProxy(scheme string, port uint16) LoadBalancer {
	return &RoundRobinProxy{
		scheme: scheme,
		port:   port,
	}
}

func (p *RoundRobinProxy) ServeHTTP(wr http.ResponseWriter, r *http.Request) {
	p.mux.RLock()
	defer p.mux.RUnlock()

	if p.total == 0 {
		// In case we didn't find any backend, return 503.
		http.Error(wr, http.StatusText(http.StatusServiceUnavailable), http.StatusServiceUnavailable)
		return
	}

	// Pick the next backend in the list, round-robin.
	n := atomic.AddInt64(&p.n, 1)
	srv := p.backends[n%p.total]
	srv.ServeHTTP(wr, r)
}

func (p *RoundRobinProxy) SetBackends(list []http.Handler) {
	p.mux.Lock()
	defer p.mux.Unlock()
	p.backends = list
	p.total = int64(len(p.backends))
	if p.cf != nil {
		// Try to get a list of the backend names.
		p.cf(httpHandlers(p.backends).String())
	}
	atomic.StoreInt64(&p.n, 0) // Technically not necessary, but still.
}

func (p *RoundRobinProxy) Scheme() string {
	return p.scheme
}

func (p *RoundRobinProxy) Port() uint16 {
	return p.port
}

func (p *RoundRobinProxy) OnUpdate(cf backendCallback) {
	p.cf = cf
}

// CookieProxy consistently hashes a cookie value to a backend bucket (sticky sessions).
type CookieProxy struct {
	backends []http.Handler
	total    uint32
	mux      sync.RWMutex // Protects backends and total.
	scheme   string
	port     uint16
	cf       backendCallback

	CookieName string
}

func NewCookieProxy(scheme string, port uint16) LoadBalancer {
	return newCookieProxy(scheme, port, "monitor-cookie")
}

func newCookieProxy(scheme string, port uint16, cookieName string) LoadBalancer {
	return &CookieProxy{
		scheme:     scheme,
		port:       port,
		CookieName: cookieName,
	}
}

func NewCookieProxyFunc(cookieName string) func(scheme string, port uint16) LoadBalancer {
	return func(scheme string, port uint16) LoadBalancer {
		return newCookieProxy(scheme, port, cookieName)
	}
}

func (p *CookieProxy) ServeHTTP(wr http.ResponseWriter, r *http.Request) {
	p.mux.RLock()
	defer p.mux.RUnlock()

	var input string // Our hash input.
	// Extract the cookie, if not found use the requester's IP address.
	cookie, err := r.Cookie(p.CookieName)
	if err != nil {
		// Use client IP.
		input = r.RemoteAddr
	} else {
		// Use the cookie's value.
		input = cookie.Value
	}
	n := crc32.ChecksumIEEE([]byte(input))
	srv := p.backends[n%p.total]
	srv.ServeHTTP(wr, r)
}

func (p *CookieProxy) SetBackends(list []http.Handler) {
	p.mux.Lock()
	defer p.mux.Unlock()
	p.backends = list
	p.total = uint32(len(p.backends))
	if p.cf != nil {
		p.cf(httpHandlers(p.backends).String())
	}
}

func (p *CookieProxy) Scheme() string {
	return p.scheme
}

func (p *CookieProxy) Port() uint16 {
	return p.port
}

func (p *CookieProxy) OnUpdate(cf backendCallback) {
	p.cf = cf
}
