package main

import (
	"bytes"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"
)

const testCSV = `Source,Target,Code
http://www.example.com/üüü,http://www.example.de/ueueue,308
http://www.example.com/,http://www.example.ch/,308
http://www.example.com/source,http://www.example.ch/target,308
http://www.example.com/regex/.*,http://www.example.ch/,308
http://www.example.com/prefix/(.*),http://www.example.ch/$1,308
http://www.example.com/foo[/]?,http://www.example.ch/,308
http://www.example.com/(.*),http://www.example.ch/$1,308

`

func testMapper(t *testing.T) domainMapper {
	m := new(mapper)
	buf := bytes.NewBufferString(testCSV)
	if err := m.Reload(buf); err != nil {
		t.Fatal(err)
	}
	return m
}

func mustRequest(tb testing.TB, method, url string, body io.Reader) *http.Request {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		tb.Fatal(err)
	}
	return req
}

func Test_handleRedirects(t *testing.T) {
	var simpleMapper = testMapper(t)

	type args struct {
		mapper domainMapper
		next   http.Handler
	}
	tests := []struct {
		name         string
		args         args
		request      *http.Request
		wantCode     int
		wantLocation string
	}{
		{
			name:         "simple mappings",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/", nil),
			wantCode:     308,
			wantLocation: "http://www.example.ch/",
		},
		{
			name:         "not found",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.de/", nil),
			wantCode:     404,
			wantLocation: "",
		},
		{
			name:         "non-ascii char in url",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/üüü", nil),
			wantCode:     308,
			wantLocation: "http://www.example.de/ueueue",
		},
		{
			name:         "non-ascii char in url 2",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/%C3%BC%C3%BC%C3%BC", nil),
			wantCode:     308,
			wantLocation: "http://www.example.de/ueueue",
		},
		{
			name:         "regex",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/regex/home", nil),
			wantCode:     308,
			wantLocation: "http://www.example.ch/",
		},
		{
			name:         "grouped regex",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/home", nil),
			wantCode:     308,
			wantLocation: "http://www.example.ch/home",
		},
		{
			name:         "query string mapping",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/?sessionID=4cb1f26a-584f-11ec-abb5-00155d1f745a", nil),
			wantCode:     308,
			wantLocation: "http://www.example.ch/?sessionID=4cb1f26a-584f-11ec-abb5-00155d1f745a",
		},
		{
			name:         "grouped regex with query string",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/home?sessionID=4cb1f26a-584f-11ec-abb5-00155d1f745a", nil),
			wantCode:     308,
			wantLocation: "http://www.example.ch/home?sessionID=4cb1f26a-584f-11ec-abb5-00155d1f745a",
		},
		{
			name:         "grouped regex with query string, stripping prefix",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/prefix/home?sessionID=4cb1f26a-584f-11ec-abb5-00155d1f745a", nil),
			wantCode:     308,
			wantLocation: "http://www.example.ch/home?sessionID=4cb1f26a-584f-11ec-abb5-00155d1f745a",
		},
		{
			name:         "regex with question mark at the end",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/foo", nil),
			wantCode:     308,
			wantLocation: "http://www.example.ch/",
		},
		{
			name:         "regex with question mark at the end",
			args:         args{mapper: simpleMapper},
			request:      mustRequest(t, http.MethodGet, "http://www.example.com/foo/", nil),
			wantCode:     308,
			wantLocation: "http://www.example.ch/",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			handler := handleRedirects(tt.args.mapper, tt.args.next)
			rr := httptest.NewRecorder()
			handler.ServeHTTP(rr, tt.request)

			res := rr.Result()
			if res.StatusCode != tt.wantCode {
				t.Errorf("wrong status code, got = %d, want = %d", res.StatusCode, tt.wantCode)
			}
			loc := res.Header.Get("Location")
			if loc != tt.wantLocation {
				t.Errorf("wrong redirect location, got = %s, want = %s", loc, tt.wantLocation)
			}
		})
	}
}

const (
	lowercase = "abcdefghijklmnopqrstuvwxyz"
	uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

func generateRandomString(letters string, n int) string {
	s := make([]rune, n)
	for i := range s {
		s[i] = rune(letters[rand.Intn(len(letters))])
	}
	return string(s)
}

// Function to generate random URL
func generateRandomURLs(domain string, n int) []string {
	const scheme = "https"
	var urls = make([]string, n)
	for i := 0; i < n; i++ {
		urls[i] = scheme + "://" + domain + "/" + generateRandomString(lowercase+uppercase, 10)
	}
	return urls
}

func BenchmarkRedirects(b *testing.B) {
	const (
		scheme = "https"
		toURL  = "https://www.example.com"
	)
	// Define the table
	benchmarks := []struct {
		numDomains       int
		numRedirectRules int
	}{
		{10, 10},
		{10, 100},
		{10, 1000},
		{10, 10000},
		{10, 100000},  // 1 million redirects
		{10, 1000000}, // 10 million redirects
		{100, 10},
		{100, 100},
		{100, 1000},
		{100, 10000},
		{100, 100000},
		{200, 10},
		{200, 100},
		{200, 1000},
		{200, 10000},
		{200, 100000},
		{300, 10},
		{300, 100},
		{300, 1000},
		{300, 10000},
		{300, 100000},
		// Add more cases as needed
	}

	for _, bm := range benchmarks {
		b.Run(fmt.Sprintf("numDomains=%d,numRedirectRules=%d", bm.numDomains, bm.numRedirectRules), func(b *testing.B) {
			// Stop the timer for setup
			b.StopTimer()

			domainMapper := new(mapper)
			domainMapper.domains = make(map[string]*serverBlock)

			// Generate random URLs
			var rules = make([]redirectRule, bm.numRedirectRules)
			var domain string
			for i := 0; i < bm.numDomains; i++ {
				block := serverBlock{
					Domain: domain,
					Rules:  rules,
				}
				domain = generateRandomString(lowercase, 15) + ".com"
				urls := generateRandomURLs(domain, bm.numRedirectRules)
				var rule redirectRule
				for k, u := range urls {
					rule = redirectRule{
						From: u,
						To:   toURL,
						Code: "308",
					}
					rules[k] = rule // We know len(rules) and len(urls) are the same.
				}
				domainMapper.domains[domain] = &block
			}

			handler := handleRedirects(domainMapper, nil)
			request := mustRequest(b, http.MethodGet, "https://"+domain, nil)

			// Start the timer for the actual benchmark
			b.StartTimer()

			// Now benchmark.
			for i := 0; i < b.N; i++ {
				handler.ServeHTTP(httptest.NewRecorder(), request)
			}
		})
	}
}
