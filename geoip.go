package main

import (
	"compress/gzip"
	"context"
	"embed"
	"fmt"
	"io"
	"io/fs"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	mmdb "github.com/oschwald/maxminddb-golang"
)

const (
	defaultGeoIPFileName = "files/dbip_country.mmdb"
	dbUrlTemplate        = "https://download.db-ip.com/free/dbip-country-lite-2006-01.mmdb.gz" // Used in time.Format()
)

//go:embed files/dbip_country.mmdb
var dbipDB embed.FS

var mmdbReader *mmdb.Reader

func initGeoIpDb(filename string) error {
	var source fs.FS
	if filename == "" {
		source = dbipDB
		filename = defaultGeoIPFileName
	} else {
		source = os.DirFS(".")
	}
	bb, err := fs.ReadFile(source, filename)
	if err != nil {
		logError("could not load GeoIP database: %v", err)
		return err
	}
	reader, err := loadGeoIpDb(bb)
	if err != nil {
		return fmt.Errorf("could not init GeoIP database: %w", err)
	}
	mmdbReader = reader
	return nil
}

func loadGeoIpDb(bb []byte) (*mmdb.Reader, error) {
	reader, err := mmdb.FromBytes(bb)
	if err != nil {
		return nil, fmt.Errorf("could not read GeoIP db: %w", err)
	}
	n := reader.Metadata.NodeCount
	logInfo("🌍 GeoIP database loaded (%d records)", n)
	return reader, nil
}

func periodicGeoIPFetch(interval time.Duration, target string) {
	tick := time.NewTicker(interval)
	for {
		func() {
			ctx, cancel := context.WithTimeout(context.Background(), interval/2)
			defer cancel()
			if target == "" {
				target = dbUrlTemplate
			}
			target := time.Now().Format(target)
			err := fetchGeoIPDb(ctx, target)
			if err != nil {
				logError("could not fetch GeoIP database: %v", err)
			}
		}()
		<-tick.C
	}
}

func fetchGeoIPDb(ctx context.Context, target string) error {
	logInfo("🌍 fetching GeoIP database from %s", target)
	req, err := http.NewRequest(http.MethodGet, target, nil)
	if err != nil {
		return fmt.Errorf("could not create request: %w", err)
	}
	req = req.WithContext(ctx)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("could not fetch GeoIP database: %w", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("could not fetch GeoIP database: %s", resp.Status)
	}
	gz, err := gzip.NewReader(resp.Body)
	if err != nil {
		return fmt.Errorf("could not decompress GeoIP database: %w", err)
	}
	defer gz.Close()
	bb, err := io.ReadAll(gz)
	if err != nil {
		return fmt.Errorf("could not decompress GeoIP database: %w", err)
	}
	reader, err := loadGeoIpDb(bb)
	if err != nil {
		return fmt.Errorf("could not init GeoIP database: %w", err)
	}
	mmdbReader = reader
	return nil
}

func getCountry(ipStr string) string {
	if mmdbReader == nil {
		return ""
	}
	ip := net.ParseIP(ipStr)
	if ip == nil {
		return ""
	}
	var result struct {
		Country struct {
			IsoCode string `maxminddb:"iso_code"`
		} `maxminddb:"country"`
	}
	err := mmdbReader.Lookup(ip, &result)
	if err != nil {
		return ""
	}
	return strings.ToUpper(result.Country.IsoCode)
}
