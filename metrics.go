package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var metricsRequests = promauto.NewCounterVec(
	prometheus.CounterOpts{
		Name: "upstream_requests_total",
		Help: "Total number of HTTP requests by upstream and status code",
	},
	[]string{"upstream", "code"},
)
