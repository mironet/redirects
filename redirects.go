package main

import (
	"fmt"
	"net"
	"net/http"
	"net/url"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"

	stdpath "path"

	"golang.org/x/net/idna"
)

type domainMap map[string]*serverBlock

func (d domainMap) Keys() []string {
	var out = make([]string, 0)
	for k := range d {
		out = append(out, k)
	}
	sort.Strings(out)
	return out
}

// redirectRule is a single redirection rule.
type redirectRule struct {
	Location  string       // The location of the request URL.
	From      string       `csv:"Source"` // Source string as in csv.
	FromURL   *url.URL     // Parsed from url.
	To        string       `csv:"Target"` // To string as in csv.
	ToURL     *url.URL     // Parsed to url.
	Code      string       `csv:"Code"` // Code to use for this redirect.
	Backend   http.Handler // If this is set, we will proxy to this backend instead of redirecting.
	Country   string       `csv:"Country"` // Country code(s).
	countries []string     // Parsed country codes.
}

// ParseURL tries to parse the url from source and target so we can use it for
// server blocks.
func (r *redirectRule) ParseURL() error {
	var err error
	r.FromURL, err = parseURL(r.From)
	if err != nil {
		return fmt.Errorf("error parsing source url %s: %w", r.From, err)
	}
	r.ToURL, err = parseURL(r.To)
	if err != nil {
		return fmt.Errorf("error parsing target url %s: %w", r.To, err)
	}
	return nil
}

func (r *redirectRule) ParseLocation() {
	loc, err := url.QueryUnescape(r.FromURL.RequestURI())
	if err != nil {
		// This should not happen since it came from a parsed URL already.
		logError("could not unescape url %s: %v", r.FromURL.RequestURI(), err)
		loc = r.FromURL.Path
	}
	if loc == "" {
		loc = "/"
	}
	r.Location = loc
}

func (r *redirectRule) ParseCountries() {
	if r.Country == "" {
		return
	}
	r.countries = strings.Split(strings.ToUpper(r.Country), ",")
	// Trim spaces.
	for i, c := range r.countries {
		r.countries[i] = strings.TrimSpace(c)
	}
}

func (r *redirectRule) MatchesCountries(req *http.Request) bool {
	if len(r.countries) == 0 {
		return true
	}
	ip := getRemoteIP(req)
	country := getCountry(ip)
	logTrace("country for ip %s is %s", ip, country)
	if country == "" {
		logWarn("could not get country for ip %s, not matching the redirect rule", ip)
		return false
	}
	// Check if the country is in the list of allowed countries.
	for _, c := range r.countries {
		if c == country {
			logTrace("country match %s, request path %s", c, req.URL.Path)
			return true
		}
	}
	logTrace("country does not match %s, request path %s", r.countries, req.URL.Path)
	return false
}

type serverBlock struct {
	Domain string
	Rules  []redirectRule
}

func parseURL(in string) (*url.URL, error) {
	u, err := url.Parse(in)
	if err != nil {
		return nil, err
	}
	return u, nil
}

type domainMapper interface {
	Get(key string) (*serverBlock, bool)
}

const regexpMeta = `\.+*?()|[]{}^$`

var (
	regexCache = make(map[string]*regexp.Regexp)
	cacheMutex sync.RWMutex
)

func getCompiledRegex(loc string) (*regexp.Regexp, error) {
	// Acquire a read lock first.
	cacheMutex.RLock()
	re, ok := regexCache[loc]
	cacheMutex.RUnlock()

	if ok {
		return re, nil
	}

	// Acquire a write lock.
	cacheMutex.Lock()
	defer cacheMutex.Unlock()

	// Double-check if the regex was stored while waiting for the lock.
	if re, ok := regexCache[loc]; ok {
		return re, nil
	}

	// Compile the regex and store it in the cache.
	re, err := regexp.Compile(loc)
	if err != nil {
		return nil, err
	}
	regexCache[loc] = re
	return re, nil
}

// next is executed only if the domain from the request's Host header is not in
// the redirect rules list.
func handleRedirects(mapper domainMapper, next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		// We never need the port in the host header. That is also how the
		// server block is defined on the mapping side.
		host, _, err := net.SplitHostPort(r.Host)
		if err != nil {
			// If there's an error, it means the host does not include a port.
			// Use the host as is.
			host = r.Host
		}
		host, err = idna.ToUnicode(host) // Could be punycode.
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}
		block, ok := mapper.Get(host)
		if !ok && next == nil {
			http.Error(rw, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return
		}
		if !ok {
			next.ServeHTTP(rw, r)
			return
		}

		// Redirect here. First let's find out if our path is in the redirect
		// list.
		for i, v := range block.Rules {
			if v.Location == r.URL.Path {
				// Country check.
				if !v.MatchesCountries(r) {
					logDebug("country check failed for %s, not using rule, index %d", r.URL.Path, i)
					continue
				}

				logDebug("matched rule %s, index %d", v.Location, i)
				if v.Backend != nil {
					logDebug("forwarding to backend %s", v.To)
					// We have a backend, let's proxy. The target is already
					// defined in the backend. If the original redirect rule
					// specified a path, it will be used by the single host
					// reverse proxy implementation in proxy.go as a default.
					v.Backend.ServeHTTP(rw, r)
					return
				}
				// Or else let's redirect.
				to := v.To
				if r.URL.RawQuery != "" {
					to = fmt.Sprintf("%s?%s", to, r.URL.RawQuery)
				}
				logDebug("redirecting %s to %s", r.URL.Path, to)
				redirect(rw, r, to, v.Code)
				return
			}
			// Let's try regex as a fallback if we find regexp meta characters
			// in the source string.
			if strings.ContainsAny(v.Location, regexpMeta) {
				// Let's try to parse as a regex and see if it would match. If
				// we did find a regexp we should include a caret at the
				// beginning of the string to make sure we match from the start
				// and a dollar sign at the end to match the end.
				loc := strings.TrimLeft(v.Location, "^")
				loc = fmt.Sprintf("^%s", loc)
				loc = strings.TrimRight(loc, "$")
				loc = fmt.Sprintf("%s$", loc)
				re, err := getCompiledRegex(loc)
				if err != nil {
					logWarn("string %s looks like regexp but does not compile: %v", v.Location, err)
					continue
				}
				path := r.URL.Path
				if re.MatchString(path) {
					// If we have a backend, let's proxy.
					if !v.MatchesCountries(r) {
						logDebug("country check failed for %s, not using rule regexp %s, index %d", r.URL.Path, v.Location, i)
						continue
					}
					logDebug("matched regex rule %s, index %d", v.Location, i)
					logTrace("regex replacement: %s -> %s", re.String(), v.To)
					to := string(re.ReplaceAll([]byte(path), []byte(v.To)))
					logTrace("replaced path %s with %s", path, to)
					if v.Backend != nil {
						// Try to parse the "to" url and extract the path so we
						// can use it in the proxy.
						u, err := parseURL(to)
						if err != nil {
							logWarn("regex: could not parse url %s: %v", to, err)
							continue
						}
						to = stdpath.Clean(u.Path)
						if u.RawQuery != "" {
							to = fmt.Sprintf("%s?%s", u.Path, u.RawQuery)
						}
						logDebug("regex: forwarding to backend %s", to)
						// For the proxied request, we use the new path with
						// regex replacements. Query strings are not handled
						// because they could also be sent in the body (e.g.
						// POST request, but also GET if too long) and we would
						// need to search and replace there too.
						r.URL.Path = to
						v.Backend.ServeHTTP(rw, r)
						return
					}
					// Else redirect.
					if r.URL.RawQuery != "" {
						to = fmt.Sprintf("%s?%s", to, r.URL.RawQuery)
					}
					logDebug("redirecting %s to %s", r.URL.Path, to)
					redirect(rw, r, to, v.Code)
					return
				}
			}
		}
		if next == nil {
			http.Error(rw, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return
		}
		// Proxy.
		next.ServeHTTP(rw, r)
	})
}

func redirect(rw http.ResponseWriter, r *http.Request, to, rcode string) {
	code, err := strconv.Atoi(rcode)
	if err != nil {
		logWarn("could not convert return code %v to int, will use 307: %v", rcode, err)
		code = http.StatusTemporaryRedirect
	}
	http.Redirect(rw, r, to, code)
}

// getRemoteIP returns the remote IP address of the request. It will try to
// parse the X-Forwarded-For header first, and if it fails, it will return the
// remote address of the request.
func getRemoteIP(r *http.Request) string {
	// Try to get the IP from the X-Forwarded-For header first.
	xff := r.Header.Get("X-Forwarded-For")
	if xff != "" {
		// The X-Forwarded-For header can contain multiple IP addresses. The
		// client's IP address will be the first one.
		ips := strings.Split(xff, ",")
		ip := strings.TrimSpace(ips[0])
		if ip != "" {
			return ip
		}
	}
	// If the X-Forwarded-For header is not set, return the remote address of
	// the request.
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return r.RemoteAddr
	}
	return ip
}
