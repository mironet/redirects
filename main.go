package main

import (
	"compress/gzip"
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"maps"
	"math"
	"net/http"
	"net/http/pprof"
	"net/url"
	"os"
	"os/signal"
	"slices"
	"strconv"
	"strings"
	"sync/atomic"
	"syscall"
	"time"

	"github.com/gorilla/handlers"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gopkg.in/fsnotify.v1"

	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

const (
	lbRoundRobin          = "round-robin"
	lbCookie              = "cookie"
	scheme                = "http"
	defaultMetricsAddress = ":8090"
)

var (
	redirectsFileName         string                                   // File name where the redirect rules are stored.
	upstream                  string                                   // Address of upstream server where to forward requests to if proxy mode is active.
	version                   = "unversioned"                          // This app's version
	address                   string                                   // Address to listen on.
	metricsAddress            string                                   // Address for the metrics server to listen on.
	loadbalance               string                = lbRoundRobin     // Loadbalancing algorithm (round-robin, cookie)
	cookieName                                      = "monitor-cookie" // Default cookie name used for lb hashing.
	usek8s                    bool                                     // Use k8s api client if possible.
	clientset                 *kubernetes.Clientset                    // k8s clientset, if used.
	insecureBackendSkipVerify bool                                     // Skip verification of backend certificates.
	versionFlag               bool                                     // Print version and exit.
	printDomainsFlag          bool                                     // Print domain set of all the rules and exit.
	geoIPFileName             string                                   // File name of the GeoIP database.
	geoIPURL                  string                                   // URL to fetch the GeoIP database from.
	geoIPUpdateInterval       time.Duration         = 24 * time.Hour   // Interval to update the GeoIP database.
)

func main() {
	flag.StringVar(&address, "l", ":8080", "Address to listen on.")
	flag.StringVar(&metricsAddress, "metrics", defaultMetricsAddress, "Address for metrics to listen on.")
	flag.StringVar(&redirectsFileName, "file", "rules.csv", "File name of redirects as CSV to watch.")
	flag.StringVar(&upstream, "upstream", "", "Upstream target for proxying. If left blank, a 404 is returned instead. Separate multiple upstreams with a comma to loadbalance. In k8s envs only specify one single target v1.Service.")
	flag.StringVar(&loadbalance, "lb", loadbalance, "Load-balancing algorithm to use (round-robin, cookie).")
	flag.StringVar(&cookieName, "cookie", cookieName, "Name of the cookie to use for load-balancing if lb mode = cookie.")
	flag.BoolVar(&usek8s, "k8s", true, "Use k8s API client to find eligible endpoints for loadbalancing if possible. If no k8s environment is detected, it falls back to direct upstream proxying.")
	flag.IntVar(&loglevel, "loglevel", loglevel, "Log level (0=trace, 1=debug, 2=info, 3=warn, 4=error).")
	flag.BoolVar(&insecureBackendSkipVerify, "insecure", false, "Skip verification of backend certificates. ⚠️")
	flag.BoolVar(&versionFlag, "version", false, "Print version and exit.")
	flag.BoolVar(&printDomainsFlag, "print-domains", false, "Print domain set of all the rules and exit.")
	flag.StringVar(&geoIPFileName, "geoip", "", "File name of the GeoIP database (if not using the embedded/online version).")
	flag.StringVar(&geoIPURL, "geoip-url", "", "URL to fetch the GeoIP database from. Passed through time.Now().Format() ⚠️")
	flag.DurationVar(&geoIPUpdateInterval, "geoip-update", geoIPUpdateInterval, "Interval to update the GeoIP database.")
	flag.Parse()

	if versionFlag {
		fmt.Printf("redirects version %s\n", version)
		return
	}

	if printDomainsFlag {
		// Load the rules file.
		m := new(mapper)
		if err := loadRedirects(redirectsFileName, m); err != nil {
			log.Fatal(err)
		}
		keys := slices.Collect(maps.Keys(m.domains))
		slices.Sort(keys)
		for _, k := range keys {
			fmt.Println(k)
		}
		return
	}

	if err := initGeoIpDb(geoIPFileName); err != nil {
		log.Fatalf("could not initialize GeoIP database: %v", err)
	}
	// Start a goroutine that will periodically update the GeoIP database.

	go periodicGeoIPFetch(geoIPUpdateInterval, geoIPURL)

	var next http.Handler = http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		http.Error(rw, http.StatusText(http.StatusNotFound), http.StatusNotFound)
	})

	logInfo("using log level %d", loglevel)

	// Get the endpoints related to the upstream.
	var newlb lbfactory
	switch loadbalance {
	case lbRoundRobin:
		newlb = NewRoundRobinProxy
	case lbCookie:
		newlb = NewCookieProxyFunc(cookieName)
	default:
		log.Fatalf("unknown load balancing algorithm: %s", loadbalance)
	}
	logInfo("⚖️ doing %s loadbalancing", loadbalance)

	var m = new(mapper)
	m.newlb = newlb
	m.backends = make(map[string]LoadBalancer)

	var namespace string
	if upstream != "" {
		stopc := make(chan struct{})
		defer close(stopc)

		// Find out if we are in a k8s cluster.
		config, err := rest.InClusterConfig()
		if err != nil && !errors.Is(err, rest.ErrNotInCluster) {
			log.Fatalf("could not load k8s config: %v", err)
		}
		if err != nil && errors.Is(err, rest.ErrNotInCluster) || !usek8s {
			logInfo("☝ we are not in a k8s cluster or do not want to use k8s")
			lb := newlb("http", 8080)
			// Try to parse upstream.
			upstreams := strings.Split(upstream, ",")
			var backends []http.Handler
			for _, up := range upstreams {
				u, err := url.Parse(up)
				if err != nil {
					log.Fatalf("could not parse upstream: %v", err)
				}
				backends = append(backends, newSingleHostReverseProxy(u, withInsecure(insecureBackendSkipVerify)))
			}
			lb.SetBackends(backends)
			next = lb
			atomic.StoreInt32(&readyz, 1) // Signal our readiness probe that we're ready.
		} else {
			// We seem to be inside a k8s cluster.
			logInfo("☸️ initializing k8s api client ...")
			clientset, err = kubernetes.NewForConfig(config)
			if err != nil {
				log.Fatalf("could not initialize k8s client: %v", err)
			}

			namespace, err = getNamespace()
			if err != nil {
				log.Fatalf("could not load namespace: %s", err)
			}
			logInfo("☸️ using namespace %s", namespace)

			m.clientset = clientset
			m.namespace = namespace
			remote, err := url.Parse(upstream) // It's supposed to be a single upstream service name.
			if err != nil {
				log.Fatalf("could not parse upstream: %v", err)
			}
			scheme, host, port := remote.Scheme, remote.Hostname(), remote.Port()

			// Create a new loadbalancer.
			portn, err := strconv.Atoi(port)
			if err != nil {
				log.Fatalf("could not parse port: %v", err)
			}
			if portn >= math.MaxUint16 {
				log.Fatalf("port %d is too large", portn)
			}
			if portn < 1 {
				log.Fatalf("port %d is too small", portn)
			}
			lb := newlb(scheme, uint16(portn))
			if lb, ok := lb.(interface{ OnUpdate(backendCallback) }); ok {
				lb.OnUpdate(func(list []string) {
					logInfo("☸️ backends updated: %s", strings.Join(list, ", "))
					if len(list) > 0 {
						atomic.StoreInt32(&readyz, 1) // Signal our readiness probe that we're ready.
					} else {
						atomic.StoreInt32(&readyz, 0) // Signal our readiness probe that we're not ready.
					}
				})
				log.Print("☸️ registered backend callback (for readiness probe)")
			}
			// Add the newly created loadbalancer to the list of backends.
			serviceName, ns := splitHostNamespace(host)
			if ns == "" {
				ns = m.namespace
			}
			serviceName = types.NamespacedName{Namespace: ns, Name: serviceName}.String()
			logDebug("k8s: backends: creating new loadbalancer for scheme=%s, portn=%d", scheme, portn)
			logTrace("k8s: backends: service name=%s, namespace=%s", serviceName, ns)
			m.backends[serviceName] = lb
			next = lb
		}
	}

	if err := loadRedirects(redirectsFileName, m); err != nil {
		log.Fatal(err)
	}

	mux := handleRedirects(m, next)
	hdl := handlers.CombinedLoggingHandler(log.Writer(), preserveRemoteIP(handlers.ProxyHeaders(mux)))
	srv, err := newServer(address, hdl)
	if err != nil {
		log.Fatal(err)
	}

	// Start listening for incoming requests.
	errc := make(chan error)
	go func() {
		logInfo("👉 starting server version %s", version)
		logInfo("👉 listening on %s", address)
		if upstream != "" {
			logInfo("👉 upstream is %s", upstream)
		}
		errc <- srv.ListenAndServe()
	}()

	// Start the metrics server.
	go func() {
		success := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// 200 Ok.
		})
		mux := http.NewServeMux()
		mux.Handle("/metrics", promhttp.Handler())
		mux.Handle("/healthz", success)
		mux.Handle("/livez", success)
		// Handle pprof requests for live profiling.
		mux.HandleFunc("/debug/pprof/", pprof.Index)
		mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
		mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		mux.HandleFunc("/debug/pprof/trace", pprof.Trace)

		// Depending on the state of the backends, we return 500 or 200. Or if
		// we don't have an upstream defined, we return 200 always since we're
		// not doing any proxying or only proxy rule-by-rule which don't have an
		// impact on  the readiness of the redirect server as a whole.
		if upstream != "" {
			mux.Handle("/readyz", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				n := atomic.LoadInt32(&readyz)
				if n < 1 {
					http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
					return
				}
				// 200 Ok.
			}))
		} else {
			mux.Handle("/readyz", success)
		}

		srv := &http.Server{
			Handler: mux,
			Addr:    metricsAddress,
		}
		logInfo("👉 metrics listening on %s", metricsAddress)
		if err := srv.ListenAndServe(); err != nil {
			logError("error in metrics server: %v", err)
		}
	}()

	// Start watching the rules file and reload in case it changes.
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		logError("error setting up file watcher: %v", err)
		return
	}
	defer watcher.Close()
	if err := watcher.Add(redirectsFileName); err != nil {
		logError("error trying to watch path: %v", err)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		logInfo("👀 start watching rules file %s", redirectsFileName)
		defer logInfo("👀 stopped watching rules file %s", redirectsFileName)
		defer cancel()
		for {
			select {
			case ev, ok := <-watcher.Events:
				if !ok {
					logInfo("fsnotify done")
					return
				}
				var reload bool
				// K8s ConfigMaps are symlinks, so the symlink is being removed
				// first and re-added later.
				if ev.Op == fsnotify.Remove {
					// Remove the watcher since the file is gone.
					if err := watcher.Remove(redirectsFileName); err != nil {
						logInfo("ℹ removing file watcher: %v", err)
					}
					time.Sleep(time.Millisecond * 100) // Wait a bit.
					// Add a watcher to the new file.
					if err := watcher.Add(redirectsFileName); err != nil {
						logError("error watching new config file: %v", err)
					}
					reload = true
				}
				if ev.Op&fsnotify.Write == fsnotify.Write {
					reload = true
				}
				if !reload {
					continue
				}
				// Reload the config.
				if err := loadRedirects(redirectsFileName, m); err != nil {
					logError("error reloading redirect rules file: %v", err)
					continue
				}
				logInfo("👍 successfully reloaded redirect rules")
			case err, ok := <-watcher.Errors:
				if !ok {
					logInfo("fsnotify done")
					return
				}
				logInfo("fsnotify error: %v", err)
			case <-ctx.Done():
				log.Print(ctx.Err())
				return
			}
		}
	}()

	term := make(chan os.Signal, 1)
	signal.Notify(term, os.Interrupt, syscall.SIGTERM)

	select {
	case sig := <-term:
		logInfo("👋 received signal %s, terminating ...", sig)
		defer cancel()
		_ = srv.Close()
	case err := <-errc:
		if err != http.ErrServerClosed {
			logInfo("server stopped with error: %v", err)
			return
		}
	}
}

var fileMux = make(chan struct{}, 1)

func loadRedirects(path string, m *mapper) error {
	select {
	case fileMux <- struct{}{}:
		time.Sleep(time.Millisecond * 100) // Throttle a bit.
		defer func() {
			<-fileMux
		}()
	default:
		return fmt.Errorf("concurrent file loading operation in progress, skipping")
	}

	file, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("could not open redirects file: %w", err)
	}
	defer file.Close()

	var stream io.Reader
	// First check if we have a gzipped file.
	buf := make([]byte, 512) // http://golang.org/pkg/net/http/#DetectContentType
	if _, err := file.Read(buf); err != nil {
		return fmt.Errorf("could not read redirects file: %w", err)
	}
	filetype := http.DetectContentType(buf)
	if _, err := file.Seek(0, 0); err != nil {
		return fmt.Errorf("could not seek in file: %w", err)
	}
	switch filetype {
	case "application/x-gzip":
		stream, err = gzip.NewReader(file)
		if err != nil {
			return fmt.Errorf("could not read gzipped file: %w", err)
		}
	default:
		stream = file
	}

	return m.Reload(stream)
}
