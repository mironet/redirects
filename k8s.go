package main

import "os"

const (
	serviceAccountNamespace = "/var/run/secrets/kubernetes.io/serviceaccount/namespace"
)

func getNamespace() (string, error) {
	data, err := os.ReadFile(serviceAccountNamespace)
	if err != nil {
		return "", err
	}
	return string(data), nil
}
