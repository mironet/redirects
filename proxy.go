package main

import (
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strconv"
	"strings"
)

type reverseProxy struct {
	proxy *httputil.ReverseProxy
	name  string
}

func (p *reverseProxy) ServeHTTP(wr http.ResponseWriter, r *http.Request) {
	p.proxy.ServeHTTP(wr, r)
}

func (p *reverseProxy) String() string {
	return p.name
}

type httpHandlers []http.Handler

func (bb httpHandlers) String() []string {
	var backends []string
	for _, b := range bb {
		if v, ok := b.(fmt.Stringer); ok {
			backends = append(backends, v.String())
			continue
		}
		backends = append(backends, fmt.Sprintf("%v", b))
	}
	return backends
}

const (
	savedRemoteIP = "X-Saved-Remote-IP"
)

func preserveRemoteIP(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		// Set the remote IP with the value passed from the proxy.
		remoteAddr := r.RemoteAddr
		// Save the original remote IP. We do this because of an interaction
		// with the gorilla logging library, which changes the RemoteAddr field
		// of the incoming request according to the X-Forwarded-For headers.
		r.Header.Set(savedRemoteIP, remoteAddr)
		h.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

// Do the same as proxyrequest.SetXForwarded but get the IP from the saved
// remote IP.
func setXForwarded(r *httputil.ProxyRequest) {
	clientIP := (r.In.Header.Get(savedRemoteIP))
	// Remove the saved remote IP from the header list.
	r.Out.Header.Del(savedRemoteIP)

	// Now continue with the stdlib behavior.
	clientIP, _, err := net.SplitHostPort(clientIP)
	if err == nil {
		prior := r.In.Header["X-Forwarded-For"]
		if len(prior) > 0 {
			clientIP = strings.Join(prior, ", ") + ", " + clientIP
		}
		r.Out.Header.Set("X-Forwarded-For", clientIP)
	} else {
		r.Out.Header.Del("X-Forwarded-For")
	}

	// Original host handling.
	if host := r.In.Header.Get("X-Forwarded-Host"); host != "" {
		r.Out.Header.Set("X-Forwarded-Host", host)
	} else {
		r.Out.Header.Set("X-Forwarded-Host", r.In.Host)
	}

	// Protocol handling.
	if proto := r.In.Header.Get("X-Forwarded-Proto"); proto != "" {
		r.Out.Header.Set("X-Forwarded-Proto", proto)
		return
	}
	if r.In.TLS == nil {
		r.Out.Header.Set("X-Forwarded-Proto", "http")
	} else {
		r.Out.Header.Set("X-Forwarded-Proto", "https")
	}
}

// Same as httputil.NewSingleHostReverseProxy but using our metrics.
func newSingleHostReverseProxy(remote *url.URL, opts ...proxyopt) http.Handler {
	proxy := &httputil.ReverseProxy{
		Rewrite: func(pr *httputil.ProxyRequest) {
			pr.SetURL(remote)
			// Set the remote IP with the value passed from the proxy.
			setXForwarded(pr)
			pr.Out.Host = pr.In.Host
		},
		ModifyResponse: func(r *http.Response) error {
			metricsRequests.WithLabelValues(remote.String(), strconv.Itoa(r.StatusCode)).Inc()
			return nil
		},
	}
	for _, opt := range opts {
		opt(proxy)
	}
	return &reverseProxy{proxy: proxy, name: remote.String()}
}

type proxyopt func(*httputil.ReverseProxy)

func withInsecure(v bool) proxyopt {
	return func(p *httputil.ReverseProxy) {
		if !v {
			return // Do nothing.
		}
		if p.Transport == nil {
			p.Transport = http.DefaultTransport
		}
		transport, ok := p.Transport.(*http.Transport)
		if !ok {
			logWarn("could not set insecure transport because the current transport is not an http.Transport")
			return // Do nothing.
		}
		if transport.TLSClientConfig == nil {
			transport.TLSClientConfig = &tls.Config{}
		}
		transport.TLSClientConfig.InsecureSkipVerify = true
	}
}
