package main

import "log"

const (
	levelTrace = iota
	levelDebug
	levelInfo
	levelWarn
	levelError
)

var (
	loglevel int = levelInfo // Log level.
)

func logTrace(format string, a ...interface{}) {
	if loglevel == levelTrace {
		log.Printf("[TRACE] "+format, a...)
	}
}

func logDebug(format string, a ...interface{}) {
	if loglevel <= levelDebug {
		log.Printf("[DEBUG] "+format, a...)
	}
}

func logInfo(format string, a ...interface{}) {
	if loglevel <= levelInfo {
		log.Printf("[INFO] "+format, a...)
	}
}

func logWarn(format string, a ...interface{}) {
	if loglevel <= levelWarn {
		log.Printf("😱 [WARN] "+format, a...)
	}
}

func logError(format string, a ...interface{}) {
	if loglevel <= levelError {
		log.Printf("❌ [ERROR] "+format, a...)
	}
}
