package main

import (
	"fmt"
	"io"
	"math"
	"net"
	"net/http"
	"net/url"
	"slices"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/smartystreets/scanners/csv"
	"golang.org/x/sync/errgroup"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/equality"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
)

const (
	keywordProxy   = "proxy"   // Use this keyword in the code column to proxy instead of redirect.
	keywordForward = "forward" // Use this keyword in the code column to forward (external to k8s cluster) instead of redirect.
)

var (
	resyncDuration time.Duration = time.Second * 60 // Time to resync k8s informer cache.
)

type mapper struct {
	mux     sync.RWMutex
	domains domainMap

	// These are used during Reload().
	clientset kubernetes.Interface // If set we will use this to find endpoints.
	namespace string               // The namespace we're running in.
	newlb     lbfactory            // The loadbalancing factory.

	// This maps service names to their backends (loadbalancers).
	backends map[string]LoadBalancer

	// If this channel is closed, the watchEndpoints will stop.
	stopCh chan struct{}
}

func (m *mapper) Get(key string) (*serverBlock, bool) {
	m.mux.RLock()
	defer m.mux.RUnlock()
	if m.domains == nil {
		m.mux.RUnlock() // We need to write, so get a write lock and discard the previous read lock.
		m.mux.Lock()
		defer m.mux.Unlock()
		m.domains = make(domainMap)
	}
	block, ok := m.domains[key]
	return block, ok
}

type schemer interface {
	Scheme() string
}

type porter interface {
	Port() uint16
}

// We start an informer to watch for changes in the endpoints in this namespace.
// This will run until Reload is called.
func (m *mapper) watchEndpoints() error {
	const scheme = "http" // The default in k8s, could be different though.
	addOrUpdateFunc := func(obj interface{}) {
		m.mux.Lock()
		defer m.mux.Unlock()
		// We only care about endpoints.
		endpoints, ok := obj.(*corev1.Endpoints)
		if !ok {
			logError("could not convert %+v to %T", obj, endpoints)
			return
		}
		// Get the service name.
		if endpoints.Namespace == "" {
			logError("addOrUpdateFunc: no namespace passed in by watcher for service %s, no updates on backends done ... loadbalancing will probably fail", endpoints.Name)
			return
		}
		serviceName := types.NamespacedName{Namespace: endpoints.Namespace, Name: endpoints.Name}.String()
		// Check if we have a backend for this service.
		lb, ok := m.backends[serviceName]
		if !ok {
			// We don't have a backend for this service, so we don't care about
			// the endpoints.
			return
		}
		// Get the endpoints.
		var endpointsList []http.Handler
		var hostList []string
		for _, v := range endpoints.Subsets {
			for _, addr := range v.Addresses {
				scheme := scheme
				if lb, ok := lb.(schemer); ok {
					scheme = lb.Scheme()
				}
				// The port as a default is the first port in the list unless it
				// is defined in the loadbalancer. In the k8s case the ports are
				// all the same for each endpoint.
				if len(v.Ports) < 1 {
					logWarn("no ports found for service %s, no updates on backends done ... loadbalancing will probably fail", serviceName)
					continue
				}
				var port uint16 = 8080
				firstp := v.Ports[0].Port
				if firstp <= math.MaxUint16 {
					port = uint16(firstp)
				}
				if lb, ok := lb.(porter); ok {
					port = lb.Port()
				}
				u := net.JoinHostPort(addr.IP, strconv.Itoa(int(port)))
				u = fmt.Sprintf("%s://%s", scheme, u)
				remote, err := url.Parse(u)
				if err != nil {
					logWarn("could not parse %s: %v", u, err)
					continue
				}
				logDebug("adding single host reverse proxy to %s", remote.String())
				endpointsList = append(endpointsList, newSingleHostReverseProxy(remote, withInsecure(true)))
				hostList = append(hostList, u)
			}
		}
		// Update the backend.
		lb.SetBackends(endpointsList)
		logInfo("☸️ (service %s) backends updated %s", serviceName, strings.Join(hostList, ", "))
	}
	deleteFunc := func(obj interface{}) {
		m.mux.Lock()
		defer m.mux.Unlock()
		// We only care about endpoints.
		endpoints, ok := obj.(*corev1.Endpoints)
		if !ok {
			logWarn("could not convert %+v to %T", obj, endpoints)
			return
		}
		// Get the service name.
		if endpoints.Namespace == "" {
			logWarn("deleteFunc: no namespace passed in by watcher for service %s, no updates on backends done ... loadbalancing will probably fail", endpoints.Name)
			return
		}
		serviceName := types.NamespacedName{Namespace: endpoints.Namespace, Name: endpoints.Name}.String()
		// Check if we have a backend for this service.
		lb, ok := m.backends[serviceName]
		if !ok {
			// We don't have a backend for this service, so we don't care about
			// the endpoints.
			return
		}
		lb.SetBackends(nil) // Remove all backends from the loadbalancer.
	}
	// Collect a unique list of namespaces based off the service names in
	// backends.
	var namespaces []string
	for k := range m.backends {
		nn, err := parseNamespacedName(k)
		if err != nil {
			logWarn("could not parse namespaced name %s: %v", k, err)
			continue
		}
		if nn.Namespace != "" {
			namespaces = append(namespaces, nn.Namespace)
		}
	}
	slices.Sort(namespaces)
	namespaces = slices.Compact(namespaces)
	logTrace("unique namespaces: %v, len: %d", namespaces, len(namespaces))
	if len(namespaces) < 1 {
		// We don't have any explicit namespaces to watch, so we just watch our
		// own namespace.
		logDebug("☸️ no namespaces to watch, watching only our own namespace %s", m.namespace)
		namespaces = append(namespaces, m.namespace)
	}
	logDebug("☸️ watching namespaces %v", namespaces)
	var g errgroup.Group
	for _, ns := range namespaces {
		ns := ns
		g.Go(func() error {
			// First create a factory for our informer.
			factory := informers.NewSharedInformerFactoryWithOptions(
				m.clientset,
				resyncDuration,
				informers.WithNamespace(ns),
			)
			// Then create an informer for the endpoints.
			endpointsInformer := factory.Core().V1().Endpoints().Informer()
			endpointsInformer.AddEventHandler(cache.ResourceEventHandlerFuncs{
				AddFunc: addOrUpdateFunc,
				UpdateFunc: func(oldObj, newObj interface{}) {
					oldEp, ok1 := oldObj.(*corev1.Endpoints)
					newEp, ok2 := newObj.(*corev1.Endpoints)
					if !ok1 || !ok2 {
						logError("could not convert %+v or %+v to %T", oldObj, newObj, oldEp)
						return
					}
					if !equality.Semantic.DeepEqual(oldEp, newEp) {
						addOrUpdateFunc(newObj)
					}
				},
				DeleteFunc: deleteFunc,
			})
			// Start the informer and let it run forever.
			go factory.Start(m.stopCh)
			for k, v := range factory.WaitForCacheSync(m.stopCh) {
				if !v {
					return fmt.Errorf("could not sync cache for %s", k)
				}
			}
			return nil
		})
	}
	return g.Wait()
}

func (m *mapper) Reload(in io.Reader) error {
	m.mux.Lock()
	defer m.mux.Unlock()
	if m.stopCh != nil { // Should not happen, but just in case.
		close(m.stopCh)
	}
	m.stopCh = make(chan struct{})

	start := time.Now()
	scanner, err := csv.NewStructScanner(in)
	if err != nil {
		return err
	}
	if scanner.ColumnScanner == nil {
		// io.EOF happened on "in".
		logWarn("not reloading anything since io.EOF reached")
		return nil
	}

	var domains = make(domainMap)
	var n int
	var line int = 1 // scanner.Scan does not count the header line.
	for scanner.Scan() {
		line++
		var rule = new(redirectRule)
		if err := scanner.Populate(rule); err != nil {
			return fmt.Errorf("could not parse csv file on line %d: %w", line, err)
		}
		if err := rule.ParseURL(); err != nil {
			// Let's skip this but log it.
			logWarn("could not parse domain on line %d: %v", line, err)
			continue
		}
		logTrace("parsed rule: %+v", rule)
		host := rule.FromURL.Hostname()
		block, ok := domains[host]
		if !ok {
			block = &serverBlock{
				Domain: host,
				Rules:  make([]redirectRule, 0),
			}
			domains[host] = block
		}
		// Set the correct location.
		rule.ParseLocation()
		// Parse the countries if present.
		rule.ParseCountries()
		switch rule.Code {
		case keywordForward:
			logTrace("rule is forwarding to: %s", rule.ToURL.String())
			// If the code is set to the special keyword "forward", we will
			// forward externally instead of redirect, even if inside a k8s
			// cluster.
			var to *url.URL
			if strings.ContainsAny(rule.Location, regexpMeta) {
				// If the location in the rule contains regexp groups, we will
				// create the single host reverse proxy "flat" (without path) to
				// prevent the Director/Rewriter of the ReverseProxy from messing
				// with the path.
				to, err = url.Parse(rule.ToURL.String())
				if err != nil {
					return fmt.Errorf("forward: could not parse url %s: %w", rule.ToURL.String(), err)
				}
				to.Path = "" // Remove the path.
				rule.Backend = newSingleHostReverseProxy(to, withInsecure(insecureBackendSkipVerify))
			} else {
				rule.Backend = newSingleHostReverseProxy(rule.ToURL, withInsecure(insecureBackendSkipVerify))
			}
		case keywordProxy:
			// If the code is set to the special keyword "proxy", we will proxy
			// instead of redirect.
			switch m.clientset == nil {
			case true:
				logTrace("rule is proxying to: %s", rule.ToURL.String())
				// We are not in a k8s cluster or we don't want to use it. Use a
				// simple single host reverse proxy.
				var to *url.URL
				if strings.ContainsAny(rule.Location, regexpMeta) {
					// If the location in the rule contains regexp groups, we will
					// create the single host reverse proxy "flat" (without path) to
					// prevent the Director/Rewriter of the ReverseProxy from messing
					// with the path.
					to, err = url.Parse(rule.ToURL.String())
					if err != nil {
						return fmt.Errorf("forward: could not parse url %s: %w", rule.ToURL.String(), err)
					}
					to.Path = "" // Remove the path.
					rule.Backend = newSingleHostReverseProxy(to, withInsecure(insecureBackendSkipVerify))
				} else {
					rule.Backend = newSingleHostReverseProxy(rule.ToURL, withInsecure(insecureBackendSkipVerify))
				}
			case false:
				logTrace("k8s (☸️): rule is proxying to: %s", rule.ToURL.String())
				var lb LoadBalancer
				// We are in a k8s cluster and we want to use it. Check if we
				// already have a backend for this service name.
				scheme, host, port := rule.ToURL.Scheme, rule.ToURL.Hostname(), rule.ToURL.Port()
				portn, err := strconv.Atoi(port)
				if err != nil {
					return fmt.Errorf("could not convert port %s to int: %w", port, err)
				}
				if portn > math.MaxUint16 {
					return fmt.Errorf("port %d is too large (> %d)", portn, math.MaxUint16)
				}
				serviceName, namespace := splitHostNamespace(host)
				if namespace == "" {
					namespace = m.namespace // Use the namespace we're running in.
				}
				serviceName = types.NamespacedName{Namespace: namespace, Name: serviceName}.String()
				if lb, ok = m.backends[serviceName]; !ok {
					// We create a new loadbalancer.
					logDebug("k8s: creating new loadbalancer for scheme=%s, portn=%d", scheme, portn)
					lb = m.newlb(scheme, uint16(portn))
					m.backends[serviceName] = lb
				}
				// Set the backend to be used in a redirect rule when it got
				// hit.
				logTrace("k8s: backends: service name=%s, namespace=%s", serviceName, namespace)
				rule.Backend = lb
			}
		}
		block.Rules = append(block.Rules, *rule)
		n++
	}

	if err := scanner.Error(); err != nil {
		return fmt.Errorf("error reading csv file: %w", err)
	}
	m.domains = domains
	if m.clientset != nil {
		// We want k8s!
		logDebug("starting to watch endpoints")
		if err := m.watchEndpoints(); err != nil {
			return fmt.Errorf("☸️ error watching endpoints: %w", err)
		}
	}

	logInfo("🧾 read %d domains and %d redirect rules from %s in %s", len(domains), n, redirectsFileName, time.Since(start))
	return nil
}

func splitHostNamespace(host string) (name string, namespace string) {
	parts := strings.Split(host, ".")
	if len(parts) < 2 {
		return host, ""
	}
	return parts[0], parts[1]
}

func parseNamespacedName(input string) (types.NamespacedName, error) {
	parts := strings.SplitN(input, "/", 2)
	if len(parts) != 2 {
		return types.NamespacedName{}, fmt.Errorf("invalid format: %s", input)
	}
	return types.NamespacedName{
		Namespace: parts[0],
		Name:      parts[1],
	}, nil
}
